package com.five.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class House {

    private String id;
    private String title;
    private Integer cId;
    private String cName;
    private String quarters;
    private String local;
    private double area;
    private double price;
    private Integer higth;
    private Integer allhigth;
    private String characteristic;
    private Integer sId;
    private String sName;
    private Integer dId;
    private String dName;
    private String details;
    private int type;
    private String aId;
    private String aName;
    private String aphone;
    private String saId;
    private String saName;
    private String saphone;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;
    private int status;
    private String img;

    public House() {
    }

    public House(String id, String title, Integer cId, String cName, String quarters, String local, double area, double price, Integer higth, Integer allhigth, String characteristic, Integer sId, String sName, Integer dId, String dName, String details, int type, String aId, String aName, String aphone, String saId, String saName, String saphone, Date time, int status, String img) {
        this.id = id;
        this.title = title;
        this.cId = cId;
        this.cName = cName;
        this.quarters = quarters;
        this.local = local;
        this.area = area;
        this.price = price;
        this.higth = higth;
        this.allhigth = allhigth;
        this.characteristic = characteristic;
        this.sId = sId;
        this.sName = sName;
        this.dId = dId;
        this.dName = dName;
        this.details = details;
        this.type = type;
        this.aId = aId;
        this.aName = aName;
        this.aphone = aphone;
        this.saId = saId;
        this.saName = saName;
        this.saphone = saphone;
        this.time = time;
        this.status = status;
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getcId() {
        return cId;
    }

    public void setcId(Integer cId) {
        this.cId = cId;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public String getQuarters() {
        return quarters;
    }

    public void setQuarters(String quarters) {
        this.quarters = quarters;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getHigth() {
        return higth;
    }

    public void setHigth(Integer higth) {
        this.higth = higth;
    }

    public Integer getAllhigth() {
        return allhigth;
    }

    public void setAllhigth(Integer allhigth) {
        this.allhigth = allhigth;
    }

    public String getCharacteristic() {
        return characteristic;
    }

    public void setCharacteristic(String characteristic) {
        this.characteristic = characteristic;
    }

    public Integer getsId() {
        return sId;
    }

    public void setsId(Integer sId) {
        this.sId = sId;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public Integer getdId() {
        return dId;
    }

    public void setdId(Integer dId) {
        this.dId = dId;
    }

    public String getdName() {
        return dName;
    }

    public void setdName(String dName) {
        this.dName = dName;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    public String getaName() {
        return aName;
    }

    public void setaName(String aName) {
        this.aName = aName;
    }

    public String getAphone() {
        return aphone;
    }

    public void setAphone(String aphone) {
        this.aphone = aphone;
    }


    public String getSaName() {
        return saName;
    }

    public void setSaName(String saName) {
        this.saName = saName;
    }

    public String getSaphone() {
        return saphone;
    }

    public void setSaphone(String saphone) {
        this.saphone = saphone;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getaId() {
        return aId;
    }

    public void setaId(String aId) {
        this.aId = aId;
    }

    public String getSaId() {
        return saId;
    }

    public void setSaId(String saId) {
        this.saId = saId;
    }

}
