package com.five.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyWebMvcConfig implements WebMvcConfigurer {
    //页面跳转
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        // 浏览器发送/test ， 就会跳转到test页面；
        registry.addViewController("/login").setViewName("login");
//        registry.addViewController("/admin").setViewName("index");
//        registry.addViewController("/admin/index").setViewName("index");
//        registry.addViewController("/admin/index.html").setViewName("index");
//        registry.addViewController("/admin/main").setViewName("main");
//        registry.addViewController("/admin/menu/toMenuManager").setViewName("menu/menuManager");
//        registry.addViewController("/admin/menu/toMenuLeftManager").setViewName("menu/menuLeft");
//        registry.addViewController("/admin/menu/toMenuRightManager").setViewName("menu/menuRight");
//        registry.addViewController("/admin/permission/toPermissionManager").setViewName("permission/permissionManager");
//        registry.addViewController("/admin/permission/toPermissionLeftManager").setViewName("permission/permissionLeft");
//        registry.addViewController("/admin/permission/toPermissionRightManager").setViewName("permission/permissionRight");
//        registry.addViewController("/admin/role/toRoleManager").setViewName("role/roleManager");
//        registry.addViewController("/admin/user/staffList").setViewName("user/staffList");
//        registry.addViewController("/admin/user/customerList").setViewName("user/customerList");
//        registry.addViewController("/admin/user/changePwd").setViewName("user/changePwd");
//        registry.addViewController("/admin/user/userInfo").setViewName("user/userInfo");
//        registry.addViewController("/admin/city/toCityManager").setViewName("configure/city");
//        registry.addViewController("/admin/newHouse/toNewHouseManager").setViewName("house/newHouseList");
//        registry.addViewController("/admin/newHouse/toSecondHouseManager").setViewName("house/secondHouseList");
//        registry.addViewController("/admin/newHouse/toRentManager").setViewName("house/rentHouseList");
//        registry.addViewController("/admin/sale/toSale").setViewName("configure/sale");
//        registry.addViewController("/admin/entrust/toSellEntrust").setViewName("entrust/sellEntrust");
//        registry.addViewController("/admin/entrust/toLeaseEntrust").setViewName("entrust/leaseEntrust");
//        registry.addViewController("/admin/decoration/toDecoration").setViewName("configure/decoration");
//        registry.addViewController("/admin/news/toNews").setViewName("news/news");
//        registry.addViewController("/admin/order/toOrder").setViewName("order/orderList");
    }
}
