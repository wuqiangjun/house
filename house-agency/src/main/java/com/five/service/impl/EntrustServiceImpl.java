package com.five.service.impl;

import com.five.common.DataGridView;
import com.five.common.UUIDUtil;
import com.five.common.UserUtil;
import com.five.domain.City;
import com.five.domain.Entrust;
import com.five.mapper.CityMapper;
import com.five.mapper.EntrustMapper;
import com.five.service.EntrustService;
import com.five.vo.EntrustVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class EntrustServiceImpl implements EntrustService {

    @Autowired
    EntrustMapper entrustMapper;

    @Autowired
    CityMapper cityMapper;

    @Override
    public List<Entrust> queryEntrust(EntrustVo entrustVo){
        List<Entrust> data = entrustMapper.queryEntrust(entrustVo);
        return data;
    }

    @Override
    public DataGridView deleteEntrust(EntrustVo entrustVo){
        try {
            entrustMapper.deleteEntrust(entrustVo);
            return DataGridView.DELETE_SUCCESS;
        }
        catch (Exception e){
            return DataGridView.DELETE_ERROR;
        }
    }

    @Override
    public DataGridView addEntrust(EntrustVo entrustVo) {
        try {
            City city = cityMapper.queryCityByName(entrustVo.getcName());
            if(city==null){
                return DataGridView.ADDERROR3;
            }
            Date date = new Date();
            entrustVo.setId(UUIDUtil.getUUID());
            entrustVo.setSaId(UserUtil.getUser().getId());
            entrustVo.setcId(city.getId());
            entrustVo.setTime(date);
            entrustMapper.addEntrust(entrustVo);
            return DataGridView.ADD_SUCCESS;
        }
        catch (Exception e){
            return DataGridView.ADD_ERROR;
        }
    }

    @Override
    public List<Entrust> queryEntrustByUser(EntrustVo entrustVo){
        entrustVo.setSaId(UserUtil.getUser().getId());
        List<Entrust> data = entrustMapper.queryAllEntrust(entrustVo);
        return data;
    }

    @Override
    public DataGridView queryEntrustNumber(Entrust entrust) {
        Long data = entrustMapper.queryEntrustNumber();
        return new DataGridView(data,"");
    }
}
