package com.five.service;

import com.five.common.DataGridView;
import com.five.common.TreeNode;
import com.five.domain.SysPermission;
import com.five.vo.PermissionVo;

import java.util.List;

public interface PermissionService {
    //查询所有可用的权限和菜单返回List
    List<SysPermission> queryAllPermission(PermissionVo permissionVo);
    //左侧导航栏业务处理
    List<TreeNode> loadIndexLeftPermissionTreeJson(PermissionVo permissionVo);
    //菜单页面左侧业务处理
    DataGridView loadLeftManagerPermissionTreeJson(PermissionVo permissionVo);
    //添加权限或菜单
    void addPermission(PermissionVo permissionVo);
    //修改权限或菜单
    void updatePermission(PermissionVo permissionVo);
    //删除权限或菜单
    void deletePermission(Integer id);
    //根据角色id查询权限或菜单
    List<SysPermission> queryPermissionByRoleId(Integer available,Integer id);
}
