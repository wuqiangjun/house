// 0 引入 用来发送请求的 方法 一定要把路径补全
import {request} from "../../request/index.js";

Page({
  data: {
    // 轮播图数组
    swiperList: [],
    houseList:[]
  },
  // 接口要的参数
  QueryParams:{
    page:1,
    limit:10
  },
  // 总页数
  totalPages:1,
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getSwiperList();
    this.getHouseList();
  },
  // 获取轮播图数据
  async getSwiperList() {
    const res=await request({url:"/user/new/allNews"});
    this.setData({
      swiperList: res.data
    })
  },
  // 获取商品列表数据
  async getHouseList(){
    const res=await request({url:"/user/house/queryHouseByNew",data:this.QueryParams});
    // 获取 总条数
    const total=res.data.count;
    // 计算总页数
    this.totalPages=Math.ceil(total/this.QueryParams.limit);
    this.setData({
      // 拼接了数组
      houseList:[...this.data.houseList,...res.data.data]
    })

    // 关闭下拉刷新的窗口 如果没有调用下拉刷新的窗口 直接关闭也不会报错  
    wx.stopPullDownRefresh();
      
  },
  // 页面上滑 滚动条触底事件
  onReachBottom(){
  //  1 判断还有没有下一页数据
    if(this.QueryParams.page>=this.totalPages){
      // 没有下一页数据
      wx.showToast({ title: '没有下一页数据' });
        
    }else{
      // 还有下一页数据
      this.QueryParams.page++;
      this.getHouseList();
    }
  },
  // 下拉刷新事件 
  onPullDownRefresh(){
    // 1 重置数组
    this.setData({
      houseList:[]
    })
    // 2 重置页码
    this.QueryParams.page=1;
    // 3 发送请求
    this.getHouseList();
  }
})