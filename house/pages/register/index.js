import {request} from "../../request/index.js";

Page({
  data: {
    isChecked:"checked"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },
  //注册
  async formSubmit(e) {
    const user = e.detail.value;
    if(user.username==""||user.username==null||
      user.password==""||user.password==null||
      user.name==""||user.name==null||
      user.sex==""||user.sex==null||
      user.phone==""||user.phone==null||
      user.identity==""||user.identity==null){
        wx.showToast({
           title: "信息不能为空",
           icon: 'none',
           mask: true
         });
      return;
    } else if(user.username.length<6||user.password.length<6){
      wx.showToast({
        title: "用户名和密码长度不能少于6位",
        icon: 'none',
        mask: true
      });
      return;
    } else if(user.phone.length<11||user.identity.length<18){
      wx.showToast({
        title: "手机号码和身份证格式错误",
        icon: 'none',
        mask: true
      });
      return;
    }
    const res = await request({ url: "/user/customer/register", method: "POST", data: user,header: { "content-type": "application/x-www-form-urlencoded" } });
    const {code,msg} = res.data;
    wx.showToast({
      title: msg,
      icon: 'none',
      mask: true
    });
    if(code===200){
      setTimeout(() => {
        // 返回上一个页面
        wx.navigateBack({
          delta: 1
        });
      }, 1000);
    }

  },
  //重置
  formReset () {
    this.setData({
      isChecked:"checked"
    })
  }
})