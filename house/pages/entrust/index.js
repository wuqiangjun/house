import {request} from "../../request/index.js";

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  // 页面跳转
  async NavigateToPages(e) {
    //创建header
    let header;
    header = {
      'content-type': 'application/x-www-form-urlencoded',
      'cookie':wx.getStorageSync("cookieKey")//读取cookie
    };
    const res = await request({ url: "/user/customer/survival", method: "POST",header: header });
    if(res.data.code===200){
      if (res && res.header && res.header['Set-Cookie']) {
        wx.setStorageSync('cookieKey', res.header['Set-Cookie']);   //保存Cookie到Storage
      }
      let id = e.currentTarget.dataset.id;
      if(id==1){
        wx.navigateTo({
          url: '../sale/index',
        })
      }else if(id==2){
        wx.navigateTo({
          url: '../rentout/index',
        })
      }
    }
    else{
      wx.navigateTo({
        url: '../login/index',
      })
    }
  },
})